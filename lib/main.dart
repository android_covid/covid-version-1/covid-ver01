import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
import 'package:covid_app/risk.dart';
import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Covid-19',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  int age = 0;
  var vaccines = ['-', '-', '-'];
  var questionScore = 0.0;
  var riskScore = 0.0;

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
    _loadRisk();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_value') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    // print(strQuestionValue.substring(1,strQuestionValue.length-1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          questionScore = questionScore + 1;
        }
      }
      questionScore = (questionScore * 100) / 11;
    });
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_value') ??
        '[false, false, false, false, false, false, false]';
    // print(strRiskValue.substring(1,strRiskValue.length-1));
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        if (arrStrRiskValues[i].trim() == 'true') {
          riskScore = riskScore + 1;
        }
      }
      riskScore = (riskScore * 100) / 7;
    });
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('vaccines');
    prefs.remove('question_value');
    prefs.remove('risk_value');
    await _loadProfile();
    await _loadQuestion();
    await _loadRisk();
  }

  Widget body = const Center(
    child: Text('My Page >-< '),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main'),
        backgroundColor: Colors.purple[200],
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(name),
                  subtitle: Text(
                    'เพศ: $gender, อายุ: $age ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'อาการ : ${questionScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (questionScore > 30)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 35,
                        color: (questionScore > 30)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ความเสี่ยง : ${riskScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (riskScore > 30)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 35,
                        color: (riskScore > 30)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ได้รับการฉีด : ${vaccines.toString()}',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: () async {
                          await _resetProfile();
                        },
                        child: const Text('Reset'))
                  ],
                ),
              ],
            ),
            color: Colors.purple[50],
          ),
          /* ListTile(
            title: Text('Profile'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Vaccine'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VaccineWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Question'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => QuestionWidget()));
              await _loadQuestion();
            },
          ),
          ListTile(
            title: Text('Risk'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Riskwidget()));
              await _loadRisk();
            },
          ), */
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text(
                'Meun',
                style: TextStyle(color: Colors.black, fontSize: 25),
              ),
              decoration: BoxDecoration(color: Colors.purple[200]),
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileWidget()));
                await _loadProfile();
              },
              tileColor: Colors.purple[50],
            ),
            ListTile(
              title: Text('Vaccine'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => VaccineWidget()));
                await _loadProfile();
              },
              tileColor: Colors.purple[50],
            ),
            ListTile(
              title: Text('Question'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => QuestionWidget()));
                await _loadQuestion();
              },
              tileColor: Colors.purple[50],
            ),
            ListTile(
              title: const Text('Risk'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Riskwidget()));
                await _loadRisk();
              },
              tileColor: Colors.purple[50],
            ),
          ],
        ),
      ),
      backgroundColor: Colors.purple[100],
    );
  }
}

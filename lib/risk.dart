import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Riskwidget extends StatefulWidget {
  Riskwidget({Key? key}) : super(key: key);

  @override
  _RiskwidgetState createState() => _RiskwidgetState();
}

class _RiskwidgetState extends State<Riskwidget> {
  var risk = [
    "โรคทางเดินหายใจเรื้อรัง",
    "โรคหัวใจและหลอดเลือด",
    "โรคไตวายเรื้อรัง",
    "โรคหลอดเลือดสมอง",
    "โรคอ้วน",
    "โรคมะเร็ง",
    "โรคเบาหวาน"
  ];
  var riskValue = [false, false, false, false, false, false, false];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Risk"),
        backgroundColor: Colors.purple[200],
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            CheckboxListTile(
              value: riskValue[0],
              title: Text(risk[0]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[0] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[1],
              title: Text(risk[1]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[1] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[2],
              title: Text(risk[2]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[2] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[3],
              title: Text(risk[3]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[3] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[4],
              title: Text(risk[4]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[4] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[5],
              title: Text(risk[5]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[5] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: riskValue[6],
              title: Text(risk[6]),
              onChanged: (newValue) {
                setState(() {
                  riskValue[6] = newValue!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: const Text('Save'),
              style: ElevatedButton.styleFrom(
                primary: Colors.purple[300], // background
                onPrimary: Colors.white, // foreground
              ),
            )
          ],
        ),
      ),
      backgroundColor: Colors.purple[100],
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_value') ??
        '[false, false, false, false, false, false, false]';
    // print(strRiskValue.substring(1,strRiskValue.length-1));
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        riskValue[i] = (arrStrRiskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_value', riskValue.toString());
  }
}
